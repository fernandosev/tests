/**
 * Teste automatizado de login
 */

// Importando bibliotecas
const assert = require('chai').assert;
const net = require('net');
const TetrinetClient = require('../client-g4/src/TetrinetClient/TetrinetClient').TetrinetClient;


// Criando conexão na mão
const client = net.createConnection({ port: 31457 }, () => {
});

var loginUser = Buffer.from([0x38, 0x30, 0x43, 0x32, 0x31, 0x30, 0x42, 0x33, 0x31, 0x33, 0x34, 0x41, 0x38, 0x35, 0x43, 0x46, 0x37, 0x31, 0x45, 0x34, 0x36, 0x46, 0x44, 0x34, 0x43, 0x31, 0x31, 0x35, 0x42, 0x38, 0x31, 0x35, 0x42, 0x32, 0x32, 0x32, 0x36, 0x43, 0x42, 0x42, 0x44, 0x39, 0x33, 0x45, 0x35, 0x37, 0x42, 0x44, 0xFF])
client.write(loginUser);

answerloginUser = '';
client.on('data', (data) => {
  answerloginUser = data.toString();
  console.log('con3');
});

// Criando conexão com o cliente
const tetrinetClient = new TetrinetClient();
tetrinetClient.connect({ username: "Ronaldo" });

answertetrinetClient = '';
tetrinetClient.onReceiveData((message) => {
  answertetrinetClient = message.toString();
  client.closeConnection();
});

// Testes Server
describe('Testando a função: Login', () => {

  // Sem Cliente
  it('Login 1 - Deve retonar uma string contendo: "winlist"', () => {

    assert(answerloginUser.toString().includes('winlist'), 'Erro');
  });
  it('Login 2 - Deve retonar uma string contendo: "akira2"', () => {

    assert(answerloginUser.toString().includes('akira2'), 'Erro');
  });

  // Com Cliente
  it('Login 3 - Deve retonar uma string contendo: "Ronaldo"', () => {

    assert(answertetrinetClient.toString().includes('Ronaldo'), 'Erro');
  });
  it('Login 4 - Deve retonar uma string contendo: "winlist"', () => {

    assert(answertetrinetClient.toString().includes('winlist'), 'Erro');
  });
});

