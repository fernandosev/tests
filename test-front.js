/**
 * Teste automatizado de login
 */

// Importando bibliotecas
const assert = require('chai').assert;
const WebSocket = require("ws");

// Mockup Server
console.log("\n\n\n\n                   *** Iniciando Teste-Front: ***\n");
console.log("Instruções: \n");
console.log("   1º - Deverá rodar o projeto frontend (react-front) com o yarn start");
console.log("   2º - Deverá rodar esse arquivo com o node test-front");
console.log("   3º - Deverá executar o máximo de comandos na interface do front (localhost:3000)");
console.log("   4º - Acompanhar os testes após 30 segundos");


var vetorMsg = [];

// Server
const wss = new WebSocket.Server({ port: 3030 });
wss.on("connection", function connection(ws) {
    ws.on("message", function incoming(data) {

        vetorMsg.push(data);

        wss.clients.forEach(function each(client) {
            client.send(data);
        });
    });
});

setTimeout(() => {

    wss.close();

    console.log('\n\n                   *** Resultados ***\n');

    down = false;
    up = false;
    right = false;
    left = false;
    startgame = false; // startgame 1
    pause = false; // pause 0
    restart = false; // pause 1
    endgame = false; // startgame 0
    pline = false;
    playerleave = false;

    for (i = 0; i < vetorMsg.length; i++) {

        msg = vetorMsg[i];

        if (msg.includes('down')) {
            down = true;
        } else if (msg.includes('up')) {
            up = true;
        } else if (msg.includes('right')) {
            right = true;
        } else if (msg.includes('left')) {
            left = true;
        } else if (msg.includes('startgame 1')) {
            startgame = true;
        } else if (msg.includes('pause 0')) {
            pause = true;
        } else if (msg.includes('pause 1')) {
            restart = true;
        } else if (msg.includes('startgame 0')) {
            endgame = true;
        } else if (msg.includes('pline')) {
            pline = true;
        } else if (msg.includes('playerleave')) {
            playerleave = true;
        }
    }

    if (down) {
        console.log('1º - Tecla para baixo funcionou');
    } else {
        console.log('1º - Tecla para baixo não funcionou');
    }
    if (up) {
        console.log('2º - Tecla para cima funcionou');
    } else {
        console.log('2º - Tecla para cima não funcionou');
    }
    if (left) {
        console.log('3º - Tecla para esquerda funcionou');
    } else {
        console.log('3º - Tecla para esquerda não funcionou');
    }
    if (right) {
        console.log('4º - Tecla para direita funcionou');
    } else {
        console.log('4º - Tecla para direita não funcionou');
    }
    if (startgame) {
        console.log('5º - Começar o jogo funcionou');
    } else {
        console.log('5º - Começar o jogo não funcionou');
    }
    if (pause) {
        console.log('6º - Pausar o jogo funcionou');
    } else {
        console.log('6º - Pausar o jogo não funcionou');
    }
    if (restart) {
        console.log('7º - Reiniciar o jogo funcionou');
    } else {
        console.log('7º - Reiniciar o jogo não funcionou');
    }
    if (endgame) {
        console.log('8º - Finalizar o jogo funcionou');
    } else {
        console.log('8º - Finalizar o jogo não funcionou');
    }
    if (pline) {
        console.log('9º - O chat funcionou');
    } else {
        console.log('9º - O chat não funcionou');
    }
    if (playerleave) {
        console.log('10º - O logout funcionou');
    } else {
        console.log('10º - O logout não funcionou');
    }

}, 30000);

